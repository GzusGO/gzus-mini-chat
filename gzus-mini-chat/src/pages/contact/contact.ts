import {Component, OnInit} from '@angular/core';
import { NavController } from 'ionic-angular';
import {UsersProvider} from "../../providers/users/users";
import {Observable} from "rxjs/Observable";
import 'rxjs/Rx';

@Component({
  selector: 'page-contact',
  templateUrl: 'contact.html'
})
export class ContactPage implements OnInit{
  user: any = {
    id: "432546",
    created: '20180110',
    user: "hi@gzus.me",
    name: "Jesus Gozalez",
    pass: "123",
    phone: "+1 (305) 984-6224"
  };
  users =[];
  userSubscription;

  constructor(public navCtrl: NavController, public usersProv:UsersProvider) {

    this.userSubscription = this.usersProv.fetchUsers(10);
    this.usersProv.users$.subscribe(x => this.users = x);

  }

  ngOnInit(){

  }

  ionViewWillUnload(){
    this.userSubscription.unsubscribe();
  }
}
