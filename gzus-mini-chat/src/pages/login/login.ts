import { Component } from '@angular/core';
import {IonicPage, NavController, ToastController} from 'ionic-angular';
import {TabsPage} from "../tabs/tabs";
import {UsersProvider} from "../../providers/users/users";
import {User} from "../../model/user.model";

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  user:string;
  pass:string;

  register: User = {
    id: "",
    created: "",
    user: "",
    name: "",
    pass: "",
    phone: ""
  };

  isRegister = false;
  registerSet = false;
  constructor(
    private navCtrl: NavController,
    private userProvider:UsersProvider,
    private toastCtrl:ToastController) {
  }

  submitLogin(user, pass){
    let out  = this.userProvider.login(user, pass);
    if (out) this.navCtrl.push(TabsPage);
    else this.toastCtrl.create({
      message: "The user must be in email format and pass more of 3 characters",
      duration: 5000
    }).present();
  }

  submitSignUp(email, name, pass, phone){
    console.log('sign up...');
    let out = this.userProvider.set(email, name, pass, phone);
    if (out) {
      this.toastCtrl.create({
        message: "user created you can log in now",
        duration: 5000
      }).present();
      this.switchToRegister();
    }
    else this.toastCtrl.create({
      message: "The user must be in email format and pass more of 3 characters, " +
      "all the information is required to set your account. ",
      duration: 5000
    }).present();
  }

  switchToRegister(){
    this.isRegister = !this.isRegister;
  }


}
