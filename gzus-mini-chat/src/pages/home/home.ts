import { Component } from '@angular/core';
import {NavController, ToastController} from 'ionic-angular';
import {Observable} from "rxjs/Observable";
import * as io from 'socket.io-client';
import {User} from "../../model/user.model";
import {UsersProvider} from "../../providers/users/users";

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  user: User;

  socket:any;
  chat_input:string;
  chats = [];
  messages = [];
  message = '';



  constructor(
     private navCtrl: NavController,
     private toastCtrl:ToastController,
     private usersProvider: UsersProvider) {

    this.user = this.usersProvider.get();
    console.log( 'user: ', this.user);
    this.socket = io('http://localhost:3000');
    this.socket.on('chat', (msg) => {
      console.log("message", msg);
      this.chats.push(msg);
    });
     this.getMessages().subscribe(message => {
      this.messages.push(message);
    });

   }

  ionicViewWillUnload() {
     this.socket.disconnect();
  }
  send(msg) {
    if(msg !=''){
      let out = this.creatMessage(msg);
      console.log("emit", out);
      this.socket.emit('chat', this.creatMessage(msg));
    }
    this.chat_input ='';
  }
  getMessages() {
    let observable =new Observable(observer => {
      this.socket.on('chat', (data) => {
        observer.next(data);
      });
    })
    return observable;
  }
  creatMessage(msg){

    let out = {
      "id": (new Date().toISOString() + "|" + this.user.user),
      "date": new Date().toISOString(),
      "by": this.user.user,
      "name" : this.user.name,
      "message": msg
    };
    console.log(out)
    return out;
  }
}
