
import { Injectable } from '@angular/core';
import {User} from "../../model/user.model";
import {Subject} from "rxjs/Subject";
import {HttpClient, HttpClientModule, HttpHeaders} from "@angular/common/http";
import {Observable} from "rxjs/Observable";
import 'rxjs/Rx';

/*
  Generated class for the UsersProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class UsersProvider {

  user$: Subject<User> = new Subject();
  users$: Subject<User[]> = new Subject();
  // isAuthenticated = false;
  token = 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1YjI4OWIwMDljNjQ3ZmI3OGJkYjAyMGQiLCJpZCI6IjQzMjU0NiIsImNyZWF0ZWQiOiIxOTcwLTAxLTAxVDAwOjAwOjAwLjAwMFoiLCJ1c2VyIjoiamVzdmVuQG1hYy5jb20iLCJuYW1lIjoiSmVzdXMgR296YWxleiIsInBhc3MiOiIxMjMiLCJwaG9uZSI6IisxICgzMDUpIDk4NC02MjI0IiwiaWF0IjoxNTI5NDMyNTY3fQ.OAjSJKu4pEO_PC1SQ9vmNcAkENSwTEvaJzyS1VZqPso';

  // private loginURL = '';
  // private UsersURL = '';
  // private registerURL = '';
  private _user: User;
  private _users;

  constructor( public http: HttpClient ) {
    console.log('Hello UsersProvider Provider');

  }

  set(email, name, pass, phone) {
    if (this.validateUser(email, name, pass, phone)) {
      let user = this._user = this.create(email, name, pass, phone);

      // http creation process

      this._user = user;
      return true;
    }  else {
      return false;
    }
  }

  get(){
    return this._user;
  }

  fetchUsers( amount ){
    let usersHeader = new HttpHeaders().set("authorization", this.token);
    amount = ((amount == "") || (amount < 1) || (amount > 100)) ? 10 : amount;
    // http request
    return  this.http.get<any>("http://localhost:3000/users", {headers: usersHeader}).subscribe(x =>{
      console.log("fetch Users: ", x.data);
      if (x.data.length > 0) {
        this._users = x.data;
        this.users$.next(x.data);
        return x.data;
      }
      else return [];
    },
      err =>{
        console.log("fetch user error: ", err);
        return 0;
      });//, {usersHeader});

  }

  login(email, pass): boolean{
    const isEmail = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    const emailValidator = (email) => {
      return ((email) && (email.length > 2 ) && (isEmail.test(email)));
    };

    this._user = this.monk();

    console.log('user on user service: ',this._user);

    if ((emailValidator(email)) && (pass.length > 3 )){

      this._user.name = this._user.user = email;
      // http request
      // emit success
      // this.isAuthenticated true
      // this.token
      // emit this.user$



      return true;
    } else {
      // emit check email format and password
      this._user.name = email;
      return false;
    }
  }
  logout(){

  }
  update(email, name, pass, phone){
    // no ready to implement
  }

  private create = (email, name, pass, phone) =>{
    return {
      id: (new Date().toISOString()) + "|" + email,
      created: (new Date().toISOString()),
      user: email,
      name: name,
      pass: pass,
      phone: phone
    };
  }

  private validateUser(email, name, pass, phone) {
    const isEmail = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    const separators = /[-\.\ \(\)\:]/g;
    const validTokens = /^[0-9\+\-\ \.\(\)]+$/;
    const isText = /^[\w\ ]+$/;
    const isPass = /^[\w*&^%$#@!]+$/;
    const phoneValidator = (phone) => {
      return ((phone) &&
        (phone) &&
        (phone.length > 3) &&
        (validTokens.test(phone)) &&
        (phone.toString().split(":")[0].replace(separators, '').length <= 14));
    };
    const emailValidator = (email) => {
      return ((email) && (email.length > 2 ) && (isEmail.test(email)));
    };
    const nameValidator = (name) => {
      return ((name) && (name.length > 2 ) && (isText.test(name)));
    }
    const passValidator = (pass) => {
      return ((pass) && (pass.length > 2 ) && (isPass.test(pass)));
    }

    if  ((emailValidator(email) &&
        (passValidator(pass)) &&
        (nameValidator(name)) &&
        (phoneValidator(phone)))) return true;
    else return false;
  }

  private monk() {
    return {
      id: "432546",
      created: '20180110',
      user: "hi@gzus.me",
      name: "Jesus Gozalez",
      pass: "123",
      phone: "+1 (305) 984-6224"
    };
  }

}
