import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import {UsersProvider} from "../../providers/users/users";
import {User} from "../../model/user.model";

@Component({
  selector: 'page-about',
  templateUrl: 'about.html'
})
export class AboutPage {

  user: User;
  constructor(private navCtrl: NavController, private userProv: UsersProvider) {
    this.user = this.userProv.get();
  }

  submitLogin(){
    console.log("update user")
  }

}
