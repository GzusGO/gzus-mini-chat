const express = require('express')
const app = express()
const bodyParser = require('body-parser')
const jwt = require('jsonwebtoken')
const socket = require('socket.io')
const config = require("./config.json")
var cors = require('cors')


const mongoose = require('mongoose');
mongoose.connect( config['db'] + "/" + config["db-name"] );

const db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function() {
    // we're connected!
    console.log("we're connected To MongoDB");

});

let messages = db.collection(config['db-messages']);
let users = db.collection(config['db-users']);

// setting server

const server =  app.listen(config['server-port'], () =>
    console.log('listening for requests on port: ' + config['server-port'] +
        '\n Host: \n\t http://' + config['server-root']+':'+ config['server-port'] +
        '\n Mongodb is set up to listen on: \n\t' + config['db']))

// middle wares:
app.use(bodyParser.json())
app.use(express.static('./public'))

// custom middleware :
const corsOptions = {
    "origin": "*",
    "methods": "GET,HEAD,PUT,PATCH,POST,DELETE",
    "preflightContinue": true,
    "optionsSuccessStatus": 204
};
app.use(cors(corsOptions))
app.use( (req,res,next) => {

    console.log(req.headers.authorization);
    if (((req.url === "/auth/login") && (req.method === "POST" )) ||
    ((req.url === "/auth/register") && (req.method === "POST" ))){
        return next();
    } else if (req.headers.authorization){

        const token = req.get('authorization').split(' ')[1];
        const decode = jwt.verify(token, config['jwt-secret-key'], (err, decoded) => {
            if (err) {
                if ((err.message) && (err.message == "invalid token"))
                    return res.status(403).json({
                        message: err.message,
                        code: 403,
                        url:req.url,
                        method: req.method });
                else
                    return res.status(400).json({
                        message: err.message,
                        code: 400,
                        url: req.url,
                        method: req.method});
            } else {
                req.user = decoded;
                return next();
            }
        });
    } else {
        return res.status(200).json({
            message: "bad request token not found",
            code: 400,
            url:req.url,
            method: req.method });
    }
});




// Components

/**
 *  Mensages
 *
 *  GET
 *  in (req, res, next)
 *
 *  req.header {
 *      authorization: "Bearer eyJhbGciO" // token public key of the user
 *      amount: 10 // an number from 1 to 100
 *
 *  }
 *  out res
 *
 *  res {
 *      data: [{}],
 *      status: {}
 *   }
 * **/

const messagesComponent = async (req, res, next) => {
    // TESTING Access to decode token:
    // console.log("jwt user decode ", req.user);


    let  amount = (req.params.amount) ?  parseInt(req.params.amount) :
        (req.headers.amount) ?  parseInt(req.headers.amount) : 10;
    amount =  ((amount > 0 ) && (amount <= 100 )) ? amount : 10;
    const user = (req.params.uid) ?  req.params.uid.toString() :
        (req.headers.uid) ?  req.headers.uid.toString() : "";
    const lastMessage = (req.params.last) ?  req.params.last.toString() :
        (req.headers.last) ?  req.headers.last.toString() : "";

    let search = {};
    if ( lastMessage != "" ) search.id = {$lt: lastMessage};
    if ( user != "" ) search.by = user;

    const msgsData =  (await messages.find(search).sort({ _id: -1 }).limit(amount).toArray()).reverse();
    const dbResp = {
        data: msgsData,
        status: {
            'type': "no error",
            'code': 200,
            'messages': "all result found"
        }
    };
    res.status(200).json(dbResp);

};

/**
 *  Users
 *  GET
 *  in (req, res, next)
 *
 *  req.header : {
 *      authorization: "Bearer eyJhbGciO" // token public key of the user
 *      amount: 10 // an number from 1 to 100
 *
 *  }
 *
 *  out res
 *
 *  res {
 *      data: [{}],
 *      status: {}
 *   }
 *
 * **/

const usersComponent = async (req, res, next) => {

    let  amount = (req.params.amount) ?  parseInt(req.params.amount) :
        (req.headers.amount) ?  parseInt(req.headers.amount) : 10;
    amount =  ((amount > 0 ) && (amount <= 100 )) ? amount : 10;
    const user = (req.params.uid) ?  req.params.uid.toString() :
        (req.headers.uid) ?  req.headers.uid.toString() : "";

    let search = {};
    if ( user != "" ) search.user = user;

    const usrsData = await users.find(search).limit(amount).toArray();
    const dbResp = {

        data: usrsData,
        status: {
            'type': "no error",
            'error-code': 200,
            'messages': "all result found"
        }
    };

    res.status(200).json(dbResp);

};

/**
 *  auth/login
 *  POST
 *  in (req, res, next)
 *
 *  req.header: {
 *      user: "hi@gzus.me",
 *      pass: "123"
 *  }
 *
 *  out res
 *
 *  res {
 *      data: [{}],
 *      status: {},
 *      token: String
 *  }
 * **/

const authLoginComponent = async (req, res, next) => {
    let dbResp = {
        data: {},
        status: {
            'type': "error",
            'code': 404,
            'messages': "user and password not found"
        }
    };

    const isEmail = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    let header = {};
    if (req) {
        header = req.headers;
        //dbResp.header = header;
        if ((req.headers.user) &&
        isEmail.test(req.headers.user)) {
            dbResp.status.messages = "valid email but user and password not found";
            let user = await users.find({'user':req.headers.user}).limit(1).toArray();
            //dbResp.data = user[0];
            if (user[0].pass == header.pass) {
                    // JWT login token here:
                const token = jwt.sign(user[0], config["jwt-secret-key"])
                    //success return
                dbResp = {
                        data: user[0],
                        status: {
                            'type': "success",
                            'code': 200,
                            'messages': "user logged in"
                        },
                        token: token
                };
            }

        }
    }


    res.status(dbResp.status.code).json(dbResp);

};

/**
 *  Auth/register
 *  POST
 *  in (req, res, next)
 *
 *
 *  req.header: {
 *     user: "hi@gzus.me",
 *     name: "Jesus Gozalez",
 *     pass: "123",
 *     phone: "+1 (305) 984-6224"
 *   }
 *
 * Save on db: {
 *     id: "432546",
 *     created: new Date(),
 *     user: "hi@gzus.me",
 *     name: "Jesus Gozalez",
 *     pass: "123",
 *     phone: "+1 (305) 984-6224"
 * }
 *
 *  out res
 *
 *  {data: {}, status: {}}
 * **/

const authRegisterComponent = async(req, res, next) => {

    // TESTING Access to decode token:
   // console.log("jwt user decode ", req.user);
  //  console.log("headers user decode ", req.headers);

    // Helpers functions:
    const isEmail = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    const separators = /[-\.\ \(\)\:]/g;
    const validTokens = /^[0-9\+\-\ \.\(\)]+$/;
    const isText = /^[\w\ ]+$/;
    const isPass = /^[\w*&^%$#@!]+$/;
    const phoneValidator = (phone) => {
        return ((phone) &&
            (phone) &&
            (phone.length > 3) &&
            (validTokens.test(phone)) &&
            (phone.toString().split(":")[0].replace(separators, '').length <= 14));
    };
    const emailValidator = (email) => {
        return ((email) && (email.length > 2 ) && (isEmail.test(email)));
    };
    const nameValidator = (name) => {
        return ((name) && (name.length > 2 ) && (isText.test(name)));
    }
    const passValidator = (pass) => {
        return ((pass) && (pass.length > 2 ) && (isPass.test(pass)));
    }

    // Def output
    let dbResp = {
        data: {},
        status: {
            'type': "error",
            'code': 400,
            'messages': "bad request check that you have: \n" +
            "{ name: String, // Only accept alphanumeric characters and spaces email \n" +
            "phone: String, // Only accept alphanumeric characters, spaces, +, -, . \n" +
            "pass: String, // Only accept alphanumeric characters, !, @, #, $, %, ^, &, *, no spaces \n" +
            "email: Stirng } // must be formatted as email \n"
        }
    };

    // Inputs
    //  create: user/email/id/uid, pass, name, phone
    //  - users/ | post
    //  - users/new | post
    //  - auth/register | post | put
    const createUser = async ( headers, dbResp) => {
        // console.log( "create in", dbResp);
        if  ((emailValidator(headers.user) &&
                (passValidator(headers.pass)) &&
                (nameValidator(headers.name)) &&
                (phoneValidator(headers.phone)))){
            users.insert(
                {
                    "id" : headers.user ,
                    "created" : new Date().toString(),
                    "user" : headers.user ,
                    "name" : headers.name,
                    "pass" : headers.pass,
                    "phone" : headers.phone
                }, function(err, data){
                    if(err) {
                        dbResp.status.messages = "Error trying to reach the database err:", err.message;

                    } else {
                        // console.log( "menage save on DB", data);
                        dbResp.data = data;
                        dbResp.status = {
                            "type": "success",
                            "code": 200,
                            "messages": "User Saved"
                        };

                    }
                    // console.log( "create out", dbResp);
                    return res.status(dbResp.status.code).json(dbResp);
                });
        } else {
            console.log( "create out: ", dbResp);
            return res.status(dbResp.status.code).json(dbResp);
        }
    };

    //  update: name, phone, pass
    //  - users/:id | put | patch
    const updateUser = async ( userOnDb, headers, dbResp)=> {

        userOnDb = userOnDb[0];
        userOnDb.name = ( nameValidator(headers.name) ) ? headers.name : userOnDb.name;
        userOnDb.pass = ( passValidator(headers.pass) ) ? headers.pass : userOnDb.pass;
        userOnDb.phone = ( phoneValidator(headers.phone) ) ? headers.phone : userOnDb.phone;
        userOnDb.updated = new Date().toString();

        if (((headers.name != "") && (nameValidator(headers.name))) ||
            ((headers.pass != "") && (nameValidator(headers.pass))) ||
            ((headers.phone != "") && (nameValidator(headers.phone))) ) {
            users.update(
                { "id" : uid },
                userOnDb,
                function(err, data){
                    if(err) {
                        dbResp.status.code = 400
                        dbResp.status.messages = "Error trying to reach the database err:", err.message;
                        console.log("updateUser error", dbResp);

                    } else {

                        dbResp.data = [];
                        dbResp.status = {
                            "type": "success",
                            "code": 200,
                            "messages": "User Saved"
                        };
                        console.log( "menage save on DB", dbResp);
                    }
                    // console.log( "update out", dbResp);
                    return res.status(dbResp.status.code).json(dbResp);
            });
        } else {
           dbResp.status.messages + "update have not valid entries";
           return res.status(dbResp.status.code).json(dbResp);
        }
    }


    // User/email/id/uid validation and return value or ""
    const getUid = () => {
        let uid = (req.params.uid) ?  req.params.uid.toString() :
            (req.headers.uid) ?  req.headers.uid.toString() :
            (req.headers.user) ? req.headers.user  : "";
        if ((uid == "") || !(emailValidator(uid))) return {val: "", msg:"user/email/uid wrong format"};
        if ((req.method != "POST") && (uid != req.user.id)) {return {val:"", msg:"wrong token", uid: uid, userId:req.user.id};}
        return uid;
    }


    // run component logic
    const uid = getUid();

    if (uid.val === "") {
        dbResp.status.messages = dbResp.status.messages + uid.msg;
        return res.status(400).json(dbResp);
    } else {
        let userOnDb = (await users.find({user: req.headers.user}).limit(1).toArray());
        const userAlreadExist = (userOnDb.length > 0);
        if ((req.method === "POST") && (userAlreadExist) ){
            dbResp.status.messages = dbResp.status.messages + "User/id/email already exist on database.";
            return res.status(403).json(dbResp);
        } else if (req.method === "POST"){
            createUser(req.headers, dbResp);
        } else if (userAlreadExist){
            updateUser(userOnDb, req.headers, dbResp);
        } else {
            return res.status(400).json(dbResp);
        }
    }

};

/**
 *  404
 *  GET
 *  in (req, res, next)
 *  out res
 * **/

const pageNotFoundComponent = (req, res, next) => {
    res.status(404).sendFile(__dirname + '/public/404.html');
};



// Routes:


app.get('/messages', (req, res, next) => messagesComponent(req, res, next));
app.get('/messages/:amount', (req, res, next) => messagesComponent(req, res, next));
app.get('/messages/:amount/last/:last', (req, res, next) => messagesComponent(req, res, next));
app.get('/users', (req, res, next) => usersComponent(req, res, next));

app.post('/users', (req, res, next) => authRegisterComponent(req, res, next));

app.get('/users/:uid', (req, res, next) => usersComponent(req, res, next));

app.put('/users/:uid', (req, res, next) => authRegisterComponent(req, res, next));
app.patch('/users/:uid', (req, res, next) => authRegisterComponent(req, res, next));

app.get('/users/:uid/messages', (req, res, next) => messagesComponent(req, res, next));
app.get('/users/:uid/messages/:amount', (req, res, next) => messagesComponent(req, res, next));

app.post('/auth/login', (req, res, next) => authLoginComponent(req, res, next));
app.put('/auth/login', (req, res, next) => authLoginComponent(req, res, next));
app.post('/auth/register', (req, res, next) => authRegisterComponent(req, res, next));
app.put('/auth/register', (req, res, next) => authRegisterComponent(req, res, next));


// all the rest of the route to 404
app.get('/*', (req, res, next) => pageNotFoundComponent(req, res, next));


// Socket setup & pass server
var io = socket(server);
io.on('connection', (socket) => {

    console.log('made socket connection', socket.id);

    // Handle chat event
    socket.on('chat', function(data){

        messages.insert(data, function(){
            console.log( "menage save on DB", data);
        });
        // console.log(data);
        io.sockets.emit('chat', data);
    });

});



